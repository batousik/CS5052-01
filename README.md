# AngularJS + Flask + Spark App

## How to Get Started

### Setup the spark cluster/standalone instance.

### Setup the App
1. Install all the necessary packages (best done inside of a virtual environment)
> pip install -r requirements.txt

2. Run the app
> python runserver.py

3. Access the App
> http://localhost:5000/blog


Credits for the template Angular JS + Flask to: shea256, https://github.com/shea256/angular-flask