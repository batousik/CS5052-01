from setuptools import setup

setup(
    name='cs5052_spark',
    version='1.0',
    description='AngularJS + Flask + Spark App',
    author='130017964',
    author_email='bbt@st-andrews.ac.uk',
    packages=['cs5052_pyspark', 'cs5052_pyspark.spark_gateway'],
    entry_points={
        'console_scripts': [
            'start-server = cs5052_pyspark.app:runserver'
        ],
    },
    install_requires=['Flask', 'Jinja2', 'simplejson'],
)
