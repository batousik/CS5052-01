import os

from spark_gateway import SparkConnector


class Api:
    def __init__(self):
        self.spark_connector = SparkConnector()

    """Error Handler"""

    @staticmethod
    def clarity_error(error):
        """Handle API errors.
            error: (string) error message
            returns: dictionary error object.
        """
        return {"result": error, }

    @staticmethod
    def get_spark_master_url():
        data = os.environ.get('MY_SPARK_MASTER', 'spark://localhost:7077')
        return data

    def initialize_spark(self, master_url):
        return self.spark_connector.initialize_spark(master_url)

    def terminate_spark(self):
        result = self.spark_connector.destroy()
        self.spark_connector = SparkConnector()
        return result

    def spark_status(self):
        return self.spark_connector.get_spark_session_status()

    def load_spark_data(self):
        return self.spark_connector.load_spark_data()

    def spark_data_status(self):
        return self.spark_connector.get_spark_data_status()

    def test_data_loaded(self):
        return self.spark_connector.test_data_loaded()

    def user(self, user_id, limit):
        return self.spark_connector.get_user(user_id, limit)

    def users(self, users_ids):
        return self.spark_connector.get_users(users_ids)

    def movies(self, request):
        return self.spark_connector.get_movies(request)

    def user_fav(self, user_id):
        return self.spark_connector.user_fav(user_id)

    def comp_user(self, user_id1, user_id2):
        return self.spark_connector.comp_user(user_id1, user_id2)

    # noinspection PyMethodMayBeStatic
    def get_file(self, filename):
        return filename
