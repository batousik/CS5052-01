(function () {
    'use strict';

    var app = angular.module('SparkAngularFlask', [
        'ngTouch',
        'ngAnimate',
        'ui.router',
        'ui-notification',
        'bsLoadingOverlay',
        'bsLoadingOverlayHttpInterceptor',
        'ui.bootstrap']);

    app.factory('myLoadingInterceptor', function (bsLoadingOverlayHttpInterceptorFactoryFactory) {
        return bsLoadingOverlayHttpInterceptorFactoryFactory({
            referenceId: 'my-loading-spinner',
            requestsMatcher: function (requestConfig) {
                return requestConfig.url.indexOf('/poll/') === -1;
            }
        });
    });

    app.factory('myErrorInterceptor', function ($q) {
        return {
            'request': function (config) {
                return config || $q.when(config);
            },
            'requestError': function (request) {
                console.log(request.status);
                return $q.reject(request);
            },
            'response': function (response) {
                if (response.status === 500) {
                    // var wnd = window.open("about:blank", "", "_blank");
                    // wnd.document.write(response.data);
                    return $q.reject(response);
                }
                return response || $q.when(response);
            },
            'responseError': function (response) {
                if (response && response.status === 401) {
                    // log
                }
                if (response && response.status === 500) {
                    return $q.reject(response);
                }
            }
        };
    });

    app.config(function (NotificationProvider, $stateProvider, $urlRouterProvider, $httpProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('landing', {
                url: '/',
                templateUrl: '/static/partials/landing/partial.html',
                controller: 'LandingController',
                controllerAs: 'vm'
            })
            .state('user', {
                url: '/user',
                templateUrl: '/static/partials/user/partial.html',
                controller: 'UserController',
                controllerAs: 'vm'
            })
            .state('users', {
                url: '/users',
                templateUrl: '/static/partials/users/partial.html',
                controller: 'UsersController',
                controllerAs: 'vm'
            })
            .state('movie', {
                url: '/movie',
                templateUrl: '/static/partials/movie/partial.html',
                controller: 'MovieController',
                controllerAs: 'vm'
            })
            .state('user_fav', {
                url: '/user_fav',
                templateUrl: '/static/partials/user_fav/partial.html',
                controller: 'UserFavController',
                controllerAs: 'vm'
            })
            .state('comp_user', {
                url: '/comp_user',
                templateUrl: '/static/partials/comp_user/partial.html',
                controller: 'CompUserController',
                controllerAs: 'vm'
            });

        NotificationProvider.setOptions({
            delay: 10000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'top'
        });

        $httpProvider.interceptors.push('myErrorInterceptor');
        $httpProvider.interceptors.push('myLoadingInterceptor');
    });

    app.run(function ($rootScope, $http, $location, bsLoadingOverlayService) {
        // redirect to login page if not logged in and trying to access a restricted page
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            $location.path('/');
        });

        bsLoadingOverlayService.setGlobalConfig({
            // delay: 0, // Minimal delay to hide loading overlay in ms.
            // activeClass: undefined, // Class that is added to the element where bs-loading-overlay is applied when the overlay is active.
            templateUrl: '/static/resource/template/loading-overlay-template.html' // Template url for overlay element. If not specified - no overlay element is created.
            // templateOptions: undefined // Options that are passed to overlay template (specified by templateUrl option above).
        });
    });
})();
