(function () {
    'use strict';

    angular
        .module('SparkAngularFlask')
        .controller('MovieController', Controller);

    function Controller($http, Notification) {
        var vm = this;

        vm.raw_response = 'none';
        vm.movie_data = [];

        vm.options = ['None', 'ASC', 'DESC'];

        vm.request = {
            ml_id: '%', //
            imdb_id: '%', //
            tmdb_id: '%', //
            title: '%', //
            genres: '', //
            year: '%', //
            orderRating: 'None',
            orderWatches: 'None',
            limit: '100' //
        };

        vm.isSuccessfull = false;

        vm.sortType = 'ml_id'; // set the default sort type
        vm.sortReverse = false;  // set the default sort order
        vm.searchFish = '';     // set the default search/filter term

        vm.get_movies = get_movies;
        vm.from_json = from_json;


        function from_json(data) {
            return angular.fromJson(data)
        }

        function get_movies(request) {
            $http.post('/api/movies_request', request,
                {
                    headers: {'Content-Type': 'application/json; charset=utf-8'}
                }
            ).then(
                function successCallback(response) {
                    var movies = from_json(response.data);
                    vm.raw_response = JSON.stringify(response.data);

                    var movie_list = [];
                    angular.forEach(movies, function (value) {
                        movie_list.push(from_json(value))
                    });
                    vm.movie_data = movie_list;
                    vm.isSuccessfull = true;
                }, function errorCallback(response) {
                    vm.isSuccessfull = false;
                    vm.movie_data = [];
                    vm.raw_response = 'Could not get movies: Ensure spark is running. ' + response.data;
                    Notification.error('Could not get movies: Ensure spark is running.')
                });
        }
    }
})();
