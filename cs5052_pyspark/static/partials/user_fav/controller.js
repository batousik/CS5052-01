(function () {
    'use strict';

    angular
        .module('SparkAngularFlask')
        .controller('UserFavController', Controller);

    function Controller($http, Notification) {
        var vm = this;

        vm.raw_response = 'none';
        vm.user_data = [];
        vm.user_id = '2';
        vm.limit = '20';
        vm.isSuccessfull = false;
        vm.count_genre = 0;

        vm.sortType = 'movie_id'; // set the default sort type
        vm.sortReverse = false;  // set the default sort order
        vm.searchFish = '';     // set the default search/filter term

        vm.get_user = get_user;
        vm.from_json = from_json;


        function from_json(data) {
            return angular.fromJson(data)
        }

        function get_user(user_id) {
            $http({
                method: 'GET',
                url: '/api/user_fav?user_id=' + user_id
            }).then(function successCallback(response) {
                var reply = from_json(response.data);
                vm.raw_response = JSON.stringify(response.data);
                var user = from_json(reply);
                var result = [];
                angular.forEach(user, function (value) {
                    result.push(from_json(value))
                });
                vm.user_data = result;
                vm.isSuccessfull = true;
            }, function errorCallback(response) {
                vm.isSuccessfull = false;
                vm.user_data = [];
                vm.raw_response = 'Could note get users: Ensure spark is running. ' + response.data;
                Notification.error('Could note get users: Ensure spark is running.')
            });
        }

    }
})();
