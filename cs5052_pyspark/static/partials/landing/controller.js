(function () {
    'use strict';

    angular
        .module('SparkAngularFlask')
        .controller('LandingController', Controller);

    function Controller($scope, $http, $timeout, Notification) {
        var vm = this;

        // Expose variables
        vm.master_url = 'spark://HOST:PORT';
        vm.files = {};
        vm.spark_status = 'Unknown';
        vm.spark_data_status = 'Unknown';

        // Load master URL
        get_master_url();

        // Expose functions
        vm.get_master_url = get_master_url;
        vm.initialize_spark = initialize_spark;
        vm.terminate_spark = terminate_spark;
        vm.load_spark_data = load_spark_data;
        vm.test_data_loaded = test_data_loaded;
        vm.to_object = to_object;

        function to_object(json) {
            return angular.fromJson(json)
        }

        var load_time = 4000, // Load the data every second
            error_count_spark = 0, // Counter for the server errors
            error_count_spark_data = 0, // Counter for the server errors
            load_promise_spark = undefined,
            load_promise_spark_data = undefined; // Pointer to the promise created by the Angular $timeout service


        function next_load_spark(mill) {
            mill = mill || load_time;
            // Always make sure the last timeout is cleared before starting a new one
            cancel_next_load_spark();
            load_promise_spark = $timeout(poll_spark_status, mill);
        }

        function next_load_spark_data(mill) {
            mill = mill || load_time;
            // Always make sure the last timeout is cleared before starting a new one
            cancel_next_load_spark_data();
            load_promise_spark_data = $timeout(poll_spark_data_status, mill);
        }

        function cancel_next_load_spark() {
            $timeout.cancel(load_promise_spark);
        }

        function cancel_next_load_spark_data() {
            $timeout.cancel(load_promise_spark_data);
        }

        function poll_spark_status() {
            $http({
                method: 'GET',
                url: '/api/poll/spark_status'
            }).then(function successCallback(response) {
                vm.spark_status = response.data;
                error_count_spark = 0;
                next_load_spark();
            }, function errorCallback() {
                vm.spark_status = 'Unknown';
                next_load_spark(++error_count_spark * 2 * load_time);
            });
        }

        function poll_spark_data_status() {
            $http({
                method: 'GET',
                url: '/api/poll/spark_data_status'
            }).then(function successCallback(response) {
                vm.spark_data_status = response.data;
                error_count_spark_data = 0;
                next_load_spark_data();
            }, function errorCallback() {
                vm.spark_data_status = 'Unknown';
                next_load_spark_data(++error_count_spark_data * 2 * load_time);
            });
        }

        // Start polling the data from the server
        poll_spark_status();
        poll_spark_data_status();

        function get_master_url() {
            $http({
                method: 'GET',
                url: '/api/spark_master_url'
            }).then(function successCallback(response) {
                vm.master_url = response.data;
            }, function errorCallback() {
                Notification.error("Could not load master URL from server")
            });
        }

        function initialize_spark(master_url) {
            $http({
                method: 'GET',
                url: '/api/initialize_spark?master_url=' + master_url
            }).then(function successCallback(response) {
                Notification.success("Initializing Spark: " + response.data)
            }, function errorCallback(response) {
                Notification.error('Failed to Initialize Spark: ' + response.data)
            });
        }

        function terminate_spark() {
            $http({
                method: 'GET',
                url: '/api/terminate_spark'
            }).then(function successCallback(response) {
                Notification.success("Terminating Spark: " + response.data)
            }, function errorCallback(response) {
                Notification.error('Failed to terminate Spark: ' + response.data)
            });
        }

        function load_spark_data() {
            $http({
                method: 'GET',
                url: '/api/load_spark_data'
            }).then(function successCallback(response) {
                Notification.success("Loading data to Spark: " + response.data)
            }, function errorCallback(response) {
                Notification.error('Failed to load data to Spark: ' + response.data)
                var wnd = window.open("Data Load Error", "", "_blank");
                wnd.document.write(response.data);
            });
        }


        vm.raw_response = 'none';
        vm.response_data = {};
        vm.isSuccessfull = false;

        function test_data_loaded() {
            $http({
                method: 'GET',
                url: '/api/test_data_loaded'
            }).then(function successCallback(response) {
                vm.raw_response = JSON.stringify(response.data);
                vm.response_data = response.data;
                Notification.success("Acquired test data")
                vm.isSuccessfull = true;
            }, function errorCallback(response) {
                vm.isSuccessfull = false;
                vm.movie_data = [];
                vm.raw_response = 'Could not get test data: Ensure spark is running. ' + response.data;
                Notification.error('Could note get test data: Ensure spark is running.')
            });
        }


        vm.table_controller = {
            'movie': {'st': 'movie_id', 'sr': false},
            'link': {'st': 'movie_id', 'sr': false},
            'rating': {'st': 'user_id', 'sr': false},
            'tag': {'st': 'user_id', 'sr': false},
            'movie_genre': {'st': 'movie_id', 'sr': false},
            'genome_score': {'st': 'user_id', 'sr': false},
            'genome_tag': {'st': 'tag_id', 'sr': false}
        };


        // Always clear the timeout when the view is destroyed, otherwise it will keep polling and leak memory
        $scope.$on("$destroy", function () {
            cancel_next_load_spark();
            cancel_next_load_spark_data();
        });
    }
})();
