(function () {
    'use strict';

    angular
        .module('SparkAngularFlask')
        .controller('UsersController', Controller);

    function Controller($http, Notification) {
        var vm = this;

        vm.raw_response = 'none';
        vm.users_data = [];
        vm.counts = [];
        vm.users_ids = '2,3,4,5';
        vm.isSuccessfull = false;

        vm.sortType = 'movie_id'; // set the default sort type
        vm.sortReverse = false;  // set the default sort order
        vm.searchFish = '';     // set the default search/filter term

        vm.sortType2 = 'user_id'; // set the default sort type
        vm.sortReverse2 = false;  // set the default sort order
        vm.searchFish2 = '';     // set the default search/filter term

        vm.get_users = get_users;
        vm.from_json = from_json;


        function from_json(data) {
            return angular.fromJson(data)
        }

        function get_users(users_ids) {
            $http({
                method: 'GET',
                url: '/api/users?users_ids=' + users_ids
            }).then(function successCallback(response) {
                var reply = from_json(response.data);
                vm.raw_response = JSON.stringify(response.data);
                var result = [];
                var result2 = [];
                var users = reply.users;
                var counts = reply.counts;
                angular.forEach(users, function (value) {
                    result.push(from_json(value))
                });

                angular.forEach(counts, function (value, key) {
                    result2.push({'user_id': key, 'cnt': from_json(value).cnt})
                });
                vm.users_data = result;
                vm.counts = result2;
                vm.isSuccessfull = true;
            }, function errorCallback(response) {
                vm.isSuccessfull = false;
                vm.users_data = [];
                vm.counts = [];
                vm.raw_response = 'Could note get users: Ensure spark is running. ' + response.data;
                Notification.error('Could note get users: Ensure spark is running.')
            });
        }

    }
})();
