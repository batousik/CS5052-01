(function () {
    'use strict';

    angular
        .module('SparkAngularFlask')
        .controller('CompUserController', Controller);

    function Controller($http, Notification) {
        var vm = this;

        vm.raw_response = 'none';
        vm.user_data = [];
        vm.user_id1 = '1';
        vm.user_id2 = '2';
        vm.limit = '20';
        vm.isSuccessfull = false;
        vm.count_genre = 0;

        vm.sortType = 'movie_id'; // set the default sort type
        vm.sortReverse = false;  // set the default sort order
        vm.searchFish = '';     // set the default search/filter term

        vm.get_user = get_user;
        vm.from_json = from_json;


        function from_json(data) {
            return angular.fromJson(data)
        }

        function get_user(user_id1, user_id2) {
            $http({
                method: 'GET',
                url: '/api/comp_user?user_id1=' + user_id1 + '&user_id2=' + user_id2
            }).then(function successCallback(response) {
                var reply = from_json(response.data);
                vm.raw_response = JSON.stringify(response.data);
                var users = from_json(reply);
                var user1 = users.user1;
                var user2 = users.user2;

                var result1 = [];
                angular.forEach(user1, function (value) {
                    result1.push(from_json(value))
                });
                vm.user_data1 = result1;

                var result2 = [];
                angular.forEach(user2, function (value) {
                    result2.push(from_json(value))
                });
                vm.user_data1 = result1;
                vm.user_data2 = result2;
                vm.isSuccessfull = true;
            }, function errorCallback(response) {
                vm.isSuccessfull = false;
                vm.user_data1 = [];
                vm.user_data2 = [];
                vm.raw_response = 'Could note get users: Ensure spark is running. ' + response.data;
                Notification.error('Could note get users: Ensure spark is running.')
            });
        }

    }
})();
