import os
import sys
from  __init__ import app

import simplejson as simplejson
from flask import send_file, request, make_response

from api import Api

api = Api()


def _convert_to_json(result):
    """Convert result object to a JSON web request."""
    response = make_response(simplejson.dumps(result, use_decimal=True))
    response.headers['Access-Control-Allow-Origin'] = "*"
    response.mimetype = "application/json"

    return response


"""Route every page request to index"""


@app.route('/')
def index():
    return send_file("templates/index.html")


"""Spark Connector API"""


@app.route('/api/spark_master_url', methods=['GET'])
def spark_master_url():
    response = api.get_spark_master_url()
    return _convert_to_json(response)


@app.route('/api/initialize_spark', methods=['GET'])
def initialize_spark():
    master_url = request.args.get('master_url')
    response = api.initialize_spark(master_url)
    return _convert_to_json(response)


@app.route('/api/terminate_spark', methods=['GET'])
def terminate_spark():
    response = api.terminate_spark()
    return _convert_to_json(response)


@app.route('/api/poll/spark_status', methods=['GET'])
def spark_status():
    response = api.spark_status()
    return _convert_to_json(response)


@app.route('/api/load_spark_data', methods=['GET'])
def load_spark_data():
    response = api.load_spark_data()
    return _convert_to_json(response)


@app.route('/api/poll/spark_data_status', methods=['GET'])
def spark_data_status():
    response = api.spark_data_status()
    return _convert_to_json(response)


"""API"""


@app.route('/api/test_data_loaded', methods=['GET'])
def test_data_loaded():
    response = api.test_data_loaded()
    return _convert_to_json(response)


@app.route('/api/user/id/<int:user_id>', methods=['GET'])
def user(user_id):
    limit = int(request.args.get('limit'))
    response = api.user(user_id, limit)
    return _convert_to_json(response)


@app.route('/api/users', methods=['GET'])
def users():
    users_ids = request.args.get('users_ids')
    response = api.users(users_ids)
    return _convert_to_json(response)


@app.route('/api/file/<string:filename>', methods=['GET'])
def get_file(filename):
    # filename = request.args.get('filename')
    # with open(os.path.join(os.environ['SPARK_HOME'], filename)) as myfile:
    #     data = myfile.read()
    # return data, 200, {'ContentType': 'text/plain'}
    response = api.get_file(filename)
    return _convert_to_json(response)


@app.route('/api/movies_request', methods=['POST'])
def movies_request():
    response = api.movies(request.get_json())
    return _convert_to_json(response)


@app.route('/api/user_fav', methods=['GET'])
def user_fav():
    user_id = request.args.get('user_id')
    response = api.user_fav(user_id)
    return _convert_to_json(response)


@app.route('/api/comp_user', methods=['GET'])
def comp_user():
    user_id1 = request.args.get('user_id1')
    user_id2 = request.args.get('user_id2')
    response = api.comp_user(user_id1, user_id2)
    return _convert_to_json(response)


# special file handlers and error handlers
@app.route('/favicon.ico')
def favicon():
    return send_file('static/resource/img/favicon.ico')


@app.errorhandler(403)
def not_authorized(e):
    return e
    # return send_file("templates/403.html")


@app.errorhandler(404)
def page_not_found(e):
    return e
    # return send_file("templates/404.html")


@app.errorhandler(500)
def internal_server_error(e):
    return e


@app.errorhandler(Exception)
def unhandled_exception(e):
    # global api
    # api.terminate_spark()
    # api = Api()
    app.logger.error('Unhandled Exception: %s', e)
    raise e.args


def runserver(host):
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host=host, port=port)


if __name__ == '__main__':
    runserver(sys.argv[1])
