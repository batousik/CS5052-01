import os

from pyspark.sql import SparkSession


def get_spark_session(spark_master_url):
    """

    :rtype: SparkSession
    """
    spark_driver_memory = "1g"
    spark_executor_memory = "1g"
    spark_executor_cores = "1"

    spark = SparkSession \
        .builder \
        .master(os.environ.get('MY_SPARK_MASTER', spark_master_url)) \
        .config("spark.driver.memory", spark_driver_memory) \
        .config("spark.executor.memory", spark_executor_memory) \
        .config("spark.executor.cores", spark_executor_cores) \
        .appName("CS5052_SPARK_APP_130017964") \
        .getOrCreate()

    return spark
