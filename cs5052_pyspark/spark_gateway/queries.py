try:
    import app
except:
    from cs5052_pyspark import app


def get_table_sql(table):
    sql = \
        '''
        SELECT * FROM global_temp.{0}
        '''.format(table)
    return sql


def get_user_sql(user_id):
    """

    :type user_id: int
    """
    sql = \
        '''
        SELECT m.movie_id,m.title,m.genre,l.imdb_id,l.tmdb_id,r.rating,r.timestamp 
        FROM   global_temp.rating r 
               RIGHT JOIN global_temp.movie m 
                       ON m.movie_id = r.movie_id 
               RIGHT JOIN global_temp.link l 
                       ON m.movie_id = l.movie_id 
        WHERE  r.user_id = {0}
        '''.format(user_id)
    return sql


def get_user_num_of_genres_sql(user_id):
    sql = \
        '''
        SELECT COUNT (DISTINCT mg.genre) AS cnt
        FROM global_temp.rating r 
             RIGHT JOIN global_temp.movie m ON r.movie_id = m.movie_id
             RIGHT JOIN global_temp.movie_genre mg ON m.movie_id = mg.movie_id
        WHERE r.user_id = {0}
        '''.format(user_id)
    return sql


def get_users_sql(users_ids):
    """

        :type users_ids: string
        """
    sql = \
        '''
        SELECT  r.user_id, m.movie_id, m.title, m.genre, l.imdb_id, l.tmdb_id, r.rating, r.timestamp
        FROM global_temp.rating r 
        RIGHT JOIN global_temp.movie m on m.movie_id = r.movie_id 
        RIGHT JOIN global_temp.link l on m.movie_id = l.movie_id 
        WHERE r.user_id IN ({0})
        '''.format(users_ids)
    return sql


def get_movies_sql(request):
    sql = \
        '''
        SELECT m.movie_id, m.title, m.genre, m.year, l.imdb_id, l.tmdb_id, 
          AVG(r.rating) AS rating, COUNT(m.movie_id) AS num_watches 
        FROM global_temp.movie m 
        RIGHT JOIN global_temp.link l ON m.movie_id = l.movie_id 
        RIGHT JOIN global_temp.rating r ON r.movie_id = m.movie_id
        WHERE m.movie_id LIKE '{0}'
          AND m.title LIKE '{1}'
          AND m.year LIKE '{2}'
          AND l.imdb_id LIKE '{3}'
          AND l.tmdb_id LIKE '{4}'
        '''.format(request['ml_id'], request['title'], request['year'], request['imdb_id'], request['tmdb_id'])

    if len(request['genres']) > 0:
        genres = request['genres'].split(',')
        for g in genres:
            sql += " AND UPPER(m.genre) LIKE '%{0}%' ".format(g.strip().upper())

    sql += ' GROUP BY m.movie_id, m.title, m.genre, m.year, l.imdb_id, l.tmdb_id '

    if request['orderWatches'] != 'None':
        sql += ' ORDER BY num_watches ' + request['orderWatches']
        if request['orderRating'] != 'None':
            sql += ', rating ' + request['orderRating']
    elif request['orderRating'] != 'None':
        sql += ' ORDER BY rating ' + request['orderRating']

    if int(request['limit']) > -1:
        sql += ' LIMIT {0} '.format(request['limit'])

    app.logger.info('Generated following sql: \n' + sql)
    return sql


def get_favourite_genre(user_id):
    sql = \
        '''
        SELECT 
          MAX(t1.rating) AS rating, t1.genre 
        FROM (
          SELECT 
            AVG(r.rating) AS rating, COUNT(r.rating) AS rcnt, mg.genre
          FROM global_temp.rating r
          RIGHT JOIN 
            global_temp.movie m on m.movie_id = r.movie_id
          RIGHT JOIN 
            global_temp.movie_genre mg on m.movie_id = mg.movie_id 
          WHERE r.user_id IN ({0})
          GROUP BY mg.genre) t1
        WHERE t1.rcnt > 10 AND rating > 3
        GROUP BY t1.genre
        
        '''.format(user_id)
    app.logger.info('Generated following sql: \n' + sql)
    return sql
