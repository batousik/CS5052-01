import os
import re
import sys
from threading import Thread, current_thread

from pyspark.sql.functions import from_unixtime
from pyspark.sql.types import StructField, StructType, StringType, IntegerType, DecimalType, DoubleType

sys.path.append('..')

try:
    import app
except:
    from cs5052_pyspark import app


tables = ['movie', 'link', 'rating', 'movie_genre']


def initialise_data(spark):
    encoding = 'UTF-8'

    path_to_resources = os.path.join(os.environ['SPARK_HOME'], 'userdata', 'ml-latest')
    ratings_path = os.path.join(path_to_resources, 'ratings.csv')
    tags_path = os.path.join(path_to_resources, 'tags.csv')
    movies_path = os.path.join(path_to_resources, 'movies.csv')
    links_path = os.path.join(path_to_resources, 'links.csv')
    genome_tags_path = os.path.join(path_to_resources, 'genome-tags.csv')
    genome_scores_path = os.path.join(path_to_resources, 'genome-scores.csv')

    #  userId=int, movieId=int, rating(0.5-5.0)=float, timestamp
    rating_struct_type = StructType([StructField('user_id', IntegerType(), False),
                                     StructField('movie_id', IntegerType(), False),
                                     StructField('rating', DecimalType(2, 1), False),
                                     StructField('timestamp', DecimalType(10, 0), False)])

    #  userId=int, movieId=int, tag=string, timestamp
    tag_struct_type = StructType([StructField('user_id', IntegerType(), False),
                                  StructField('movie_id', IntegerType(), False),
                                  StructField('tag', StringType(), False),
                                  StructField('timestamp', DecimalType(10, 0), False)])

    # movieId=int, imdbId=string, tmdbId=string
    link_struct_type = StructType([StructField('movie_id', IntegerType(), False),
                                   StructField('imdb_id', StringType(), False),
                                   StructField('tmdb_id', StringType(), False)])

    # movieId=int, tagId=int, relevance=float
    genome_score_struct_type = StructType([StructField('user_id', IntegerType(), False),
                                           StructField('tag_id', IntegerType(), False),
                                           StructField('relevance', DoubleType(), False)])

    # tagId=int, tag=string
    genome_tag_struct_type = StructType([StructField('tag_id', IntegerType(), False),
                                         StructField('tag', StringType(), False)])

    # movieId=int, title=string, genres = string, or enum?
    movie_temp_struct_type = StructType([StructField('movie_id', IntegerType(), False),
                                         StructField('title', StringType(), False),
                                         StructField('genre', StringType(), False)])
    threads = [
        Thread(target=read_ratings, args=(spark, encoding, ratings_path, rating_struct_type)),
        # Thread(target=read_tags, args=(spark, encoding, tags_path, tag_struct_type)),
        Thread(target=read_links, args=(spark, encoding, links_path, link_struct_type)),
        # Thread(target=read_genome_scores, args=(spark, encoding, genome_scores_path, genome_score_struct_type)),
        # Thread(target=read_genome_tags, args=(spark, encoding, genome_tags_path, genome_tag_struct_type)),
        Thread(target=read_movies, args=(spark, encoding, movies_path, movie_temp_struct_type)),
    ]

    for thread in threads:
        thread.start()

    app.logger.info('Thread ' + current_thread().name + ': Loading data')

    for thread in threads:
        thread.join()


def read_ratings(spark, encoding, path, struct_type):
    ratings_temp = spark.read.csv(path=path, schema=struct_type, encoding=encoding)
    ratings = ratings_temp.select(ratings_temp.user_id, ratings_temp.movie_id, ratings_temp.rating,
                                  from_unixtime(ratings_temp.timestamp).alias('timestamp'))
    ratings.createGlobalTempView('rating')
    app.logger.info('Thread ' + current_thread().name + ': Loaded Ratings Data')


def read_tags(spark, encoding, path, struct_type):
    tags_temp = spark.read.csv(path=path, schema=struct_type, encoding=encoding)
    tags = tags_temp.select(tags_temp.user_id, tags_temp.movie_id, tags_temp.tag,
                            from_unixtime(tags_temp.timestamp).alias('timestamp'))
    tags.createGlobalTempView('tag')

    app.logger.info('Loaded Tags Data')


def read_links(spark, encoding, path, struct_type):
    links = spark.read.csv(path=path, schema=struct_type, encoding=encoding)
    links.createGlobalTempView('link')
    app.logger.info('Thread ' + current_thread().name + ': Loaded Links Data')


def read_genome_scores(spark, encoding, path, struct_type):
    genome_scores = spark.read.csv(path=path, schema=struct_type,
                                   encoding=encoding)
    genome_scores.createGlobalTempView('genome_score')
    app.logger.info('Thread ' + current_thread().name + ': Loaded Genome Scores Data')


def read_genome_tags(spark, encoding, path, struct_type):
    genome_tags = spark.read.csv(path=path, schema=struct_type,
                                 encoding=encoding)
    genome_tags.createGlobalTempView('genome_tag')
    app.logger.info('Thread ' + current_thread().name + ': Loaded Genome Tags Data')


def read_movies(spark, encoding, path, struct_type):
    temp_movies = spark.read.csv(path=path, schema=struct_type, encoding=encoding)
    # movies = temp_movies

    rdd_split = temp_movies.rdd.map(lambda (x, y, z): (x, y, z.split('|')))
    rdd_explode = rdd_split.flatMap(lambda (x, y, z): [(x, y, k) for k in z])
    df_final = rdd_explode.toDF(['movie_id', 'title', 'genre'])

    rdd_map = temp_movies.rdd.map(lambda (x, y, z): (
        x, y, z,
        re.sub(r'^\(|\)$', '', re.search('(\(\d{4}\))', y).group()) if re.search('(\(\d{4}\))', y) else '0000'))
    movies = rdd_map.toDF(['movie_id', 'title', 'genre', 'year'])

    movie_genre = df_final.drop('title')

    movies.createGlobalTempView('movie')
    movie_genre.createGlobalTempView('movie_genre')
    app.logger.info('Thread ' + current_thread().name + ': Loaded Movies Data')
