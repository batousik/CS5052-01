import threading
import unicodedata
from threading import Thread
from uuid import uuid4

from pyspark.sql.functions import rand

from data_initializr import initialise_data, tables
from queries import *
from spark_initializr import get_spark_session


def from_unicode(value):
    return unicodedata.normalize('NFKD', value).encode('utf-8', 'ignore')


def uid():
    return 't' + str(uuid4()).replace('-', '')


class SparkConnector:
    def __init__(self):
        self.spark_session = None
        self.data_status = 'No Data'
        self.spark_session_status = 'Terminated'

    def initialize_spark(self, master_url):
        if self.spark_session is None:
            self.spark_session_status = 'Running'
            self.spark_session = get_spark_session(master_url)
            return 'Created Spark Context Successfully'
        return 'Spark Context Already exists!'

    def get_spark_session_status(self):
        return self.spark_session_status

    def load_spark_data(self):
        if self.spark_session is None:
            return 'Spark context is not created or was destroyed!'

        if self.data_status == 'No Data':
            try:
                initialise_data(self.spark_session)
                self.data_status = 'Data Loaded'
                threading.Thread(target=self.test_print_dataset).start()
                return 'Data was initialized successfully'
            except Exception as inst:
                print inst.args
                raise Exception('Exception Loading data: ' + inst.args)
        return 'Data has been already loaded!'

    def get_spark_data_status(self):
        return self.data_status

    def destroy(self):
        if self.spark_session is None:
            return 'Spark context is not created or was destroyed!'
        self.spark_session_status = 'Terminated'
        self.spark_session.stop()
        self.spark_session = None
        self.data_status = 'No Data'
        return 'Spark Context was destroyed'

    def __get_table_as_json(self, table):
        return self.spark_session.sql(get_table_sql(table)).toJSON().collect()

    def test_print_dataset(self):
        for table in tables:
            query = get_table_sql(table)
            # print self.spark_session
            print 'Will print: ' + table
            self.spark_session.sql(query).orderBy(rand()).show()

    def test_data_loaded(self):
        result = {}
        for table in tables:
            query = get_table_sql(table)
            try:
                result[table] = self.spark_session.sql(query).orderBy(rand()).toJSON().take(20)
            except Exception:
                app.logger.error(Exception.args)
                raise Exception
        return result

    def get_user(self, user_id, limit=40):
        # get the user info
        user_query = get_user_sql(user_id)
        user_count_genre_query = get_user_num_of_genres_sql(user_id)

        user_uuid = uid()
        user_count_genre_uuid = uid()

        user_thread = Thread(target=self.__get_user_user,
                             args=(user_uuid, user_query))
        user_count_genre_thread = Thread(target=self.__get_user_count_genre,
                                         args=(user_count_genre_uuid, user_count_genre_query))

        user_thread.start()
        user_count_genre_thread.start()

        user_print_thread = Thread(target=self.__get_user_user_print,
                                   args=(user_query,))
        user_count_genre_print_thread = Thread(target=self.__get_user_count_genre_print,
                                               args=(user_count_genre_query,))

        user_print_thread.start()
        user_count_genre_print_thread.start()

        user_thread.join()
        user_count_genre_thread.join()

        result = {'user': self.__get_table_as_json(user_uuid),
                  'count_genre': self.__get_table_as_json(user_count_genre_uuid)}
        return result

    def __get_user_user(self, uuid, query):
        app.logger.info('Thread ' + threading.current_thread().name + ': Retrieving user')
        self.spark_session.sql(query).createGlobalTempView(uuid)

    def __get_user_user_print(self, query):
        app.logger.info('Thread ' + threading.current_thread().name + ': Printing user')
        self.spark_session.sql(query).show(1, False)

    def __get_user_count_genre(self, uuid, query):
        app.logger.info('Thread ' + threading.current_thread().name + ': Printing user count genre')
        self.spark_session.sql(query).createGlobalTempView(uuid)

    def __get_user_count_genre_print(self, query):
        app.logger.info('Thread ' + threading.current_thread().name + ': Retrieving user count genre')
        self.spark_session.sql(query).show()

    def get_users(self, users_ids):
        result = {}

        # get the users info
        data = from_unicode(users_ids)
        query = get_users_sql(users_ids)
        x = self.spark_session.sql(query).toJSON().collect()

        result['users'] = x
        data_list = []
        data_dict = {}
        [data_list.append(x.strip()) for x in data.split(',')]
        for data in data_list:
            query = get_user_num_of_genres_sql(int(data))
            # self.spark_session.sql(query).show()
            data_dict[data] = self.spark_session.sql(query).toJSON().first()

        result['counts'] = data_dict

        return result

    def get_movies(self, request):
        request1 = {}
        for key, value in request.iteritems():
            if isinstance(value, bool):
                request1[from_unicode(key)] = value
            else:
                request1[from_unicode(key)] = from_unicode(value)

        query = get_movies_sql(request1)
        result = self.spark_session.sql(query).toJSON().collect()
        return result

    def user_fav(self, user_id):
        query = get_favourite_genre(user_id)
        result = self.spark_session.sql(query).toJSON().collect()
        return result

    def comp_user(self, user_id1, user_id2):
        query = get_favourite_genre(user_id1)
        result1 = self.spark_session.sql(query).toJSON().collect()
        query = get_favourite_genre(user_id2)
        result2 = self.spark_session.sql(query).toJSON().collect()

        result = {'user1': result1, 'user2': result2}

        return result
